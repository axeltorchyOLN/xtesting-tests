# Différents tests Xtesting

## Organisation des tests dans `site.yml`

L'objectif est de lancer le playbook `site.yml` avec différentes façons
d'organiser les tests, afin de voir comment cela se traduit en termes
de stages/jobs dans le pipeline généré sur le projet Gitlab.

On regardera aussi l'influence de différents paramètres : `SEQUENTIALLY`,
`project_name`, `project`...
